#!/bin/sh

set -e

mix deps.get

yarn --cwd assets

mix ecto.setup

mix phx.server
