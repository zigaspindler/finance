defmodule Finance.Repo.Migrations.CreateClients do
  use Ecto.Migration

  def change do
    create table(:clients) do
      add :name, :string
      add :street, :string
      add :city, :string
      add :zip, :string
      add :state, :string
      add :country, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:clients, [:user_id])
  end
end
