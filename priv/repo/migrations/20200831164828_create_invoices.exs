defmodule Finance.Repo.Migrations.CreateInvoices do
  use Ecto.Migration

  def change do
    create table(:invoices) do
      add :description, :string, null: false
      add :date, :date, null: false
      add :due, :date, null: false
      add :user_id, references(:users, on_delete: :delete_all)
      add :client_id, references(:clients, on_delete: :delete_all)

      timestamps()
    end

    create index(:invoices, [:user_id])
    create index(:invoices, [:client_id])
  end
end
