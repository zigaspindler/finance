defmodule Finance.Repo.Migrations.CreateIncomes do
  use Ecto.Migration

  def change do
    create table(:incomes) do
      add :description, :string, null: false
      add :amount, :money_with_currency, null: false
      add :date, :date, null: false
      add :client_id, references(:clients, on_delete: :delete_all)
      add :invoice_id, references(:invoices, on_delete: :nilify_all)

      timestamps()
    end

    create index(:incomes, [:client_id])
    create index(:incomes, [:invoice_id])
  end
end
