defmodule Finance.Clients.Client do
  use Ecto.Schema
  import Ecto.Changeset

  schema "clients" do
    field :city, :string
    field :country, :string
    field :name, :string
    field :state, :string
    field :street, :string
    field :zip, :string

    belongs_to :user, Finance.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(client, attrs) do
    client
    |> cast(attrs, [:name, :street, :city, :zip, :state, :country])
    |> validate_required([:name, :street, :city, :zip, :state, :country, :user_id])
  end
end
