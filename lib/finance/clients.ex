defmodule Finance.Clients do
  import Ecto.Query, warn: false

  alias Finance.Repo

  alias Finance.Clients.Client

  def get_clients_for_user(user_id) do
    query = from c in Client, where: c.user_id == ^user_id

    Repo.all(query)
  end

  def create_client_for_user(attrs, user_id) do
    %Client{user_id: user_id}
    |> Client.changeset(attrs)
    |> Repo.insert()
  end

  def change_client(%Client{} = client, attrs \\ %{}) do
    Client.changeset(client, attrs)
  end
end
