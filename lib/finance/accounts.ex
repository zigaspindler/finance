defmodule Finance.Accounts do
  import Ecto.Query, warn: false

  alias Finance.Repo

  alias Finance.Accounts.User

  def find_or_create_user_by_email(%{
        "email" => email,
        "given_name" => first_name,
        "family_name" => last_name
      }) do
    {:ok, user} =
      %User{}
      |> User.changeset(%{email: email, first_name: first_name, last_name: last_name})
      |> Repo.insert(on_conflict: :nothing)

    if is_nil(user.id) do
      Repo.one(from u in User, where: u.email == ^email)
    else
      user
    end
  end

  def get_user(id), do: Repo.get(User, id)
end
