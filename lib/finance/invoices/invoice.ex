defmodule Finance.Invoices.Invoice do
  use Ecto.Schema
  import Ecto.Changeset

  schema "invoices" do
    field :date, :date
    field :due, :date
    field :description, :string

    belongs_to :user, Finance.Accounts.User
    belongs_to :client, Finance.Clients.Client
    has_many :incomes, Finance.Invoices.Income

    timestamps()
  end

  @doc false
  def changeset(invoice, attrs) do
    invoice
    |> cast(attrs, [:date, :due, :client_id, :description])
    |> validate_required([:date, :due, :user_id, :client_id, :incomes, :description])
  end
end
