defmodule Finance.Invoices.Income do
  use Ecto.Schema
  import Ecto.Changeset

  schema "incomes" do
    field :amount, Money.Ecto.Composite.Type
    field :date, :date
    field :description, :string

    belongs_to :client, Finance.Clients.Client
    belongs_to :invoice, Finance.Invoices.Invoice

    timestamps()
  end

  @doc false
  def changeset(income, attrs) do
    attrs =
      attrs
      |> cast_amount()

    income
    |> cast(attrs, [:description, :amount, :date, :client_id])
    |> validate_required([:description, :amount, :date, :client_id])
    |> validate_amount(:amount)
  end

  defp cast_amount(%{"amount_amount" => amount, "amount_currency" => currency} = attrs) do
    {:ok, combined_amount} = Money.parse(amount, String.to_atom(currency))
    Map.put(attrs, "amount", combined_amount)
  end

  defp cast_amount(attrs), do: attrs

  defp validate_amount(changeset, field) do
    validate_change(changeset, field, fn
      _, %Money{amount: amount} when amount > 0 -> []
      _, _ -> [amount: "must be greater than 0"]
    end)
  end
end
