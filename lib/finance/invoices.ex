defmodule Finance.Invoices do
  import Ecto.Query, warn: false

  alias Finance.Repo

  alias Finance.Accounts.User
  alias Finance.Clients.Client

  alias Finance.Invoices.{
    Income,
    Invoice
  }

  def get_incomes_for_user(user_id) do
    query =
      from i in Income,
        join: c in Client,
        on: i.client_id == c.id,
        where: c.user_id == ^user_id,
        preload: [:client]

    Repo.all(query)
  end

  def change_income(%Income{} = income, attrs \\ %{}) do
    Income.changeset(income, attrs)
  end

  def create_income(attrs) do
    %Income{}
    |> Income.changeset(attrs)
    |> Repo.insert()
  end

  def get_income(income_id, user_id) do
    query =
      from i in Income,
        join: c in Client,
        on: i.client_id == c.id,
        where: c.user_id == ^user_id and i.id == ^income_id

    Repo.one(query)
  end

  def get_incomes_by_ids(ids, user_id) do
    query =
      from i in Income,
        join: c in Client,
        on: i.client_id == c.id,
        where: c.user_id == ^user_id and i.id in ^ids

    Repo.all(query)
  end

  def update_income(id, user_id, attrs) do
    id
    |> get_income(user_id)
    |> Income.changeset(attrs)
    |> Repo.update()
  end

  def delete_income(id, user_id) do
    id
    |> get_income(user_id)
    |> Repo.delete()
  end

  def get_uninvoiced_incomes(client_id, user_id) do
    query =
      from i in Income,
        join: c in Client,
        on: i.client_id == c.id,
        where: c.user_id == ^user_id and i.client_id == ^client_id and is_nil(i.invoice_id)

    Repo.all(query)
  end

  def get_invoices_for_user(user_id) do
    query =
      from i in Invoice,
        where: i.user_id == ^user_id,
        preload: [:client]

    Repo.all(query)
  end

  def get_invoice(id, user_id) do
    query =
      from i in Invoice,
        where: i.user_id == ^user_id and i.id == ^id

    Repo.one(query)
  end

  def change_invoice(%Invoice{} = invoice, attrs \\ %{}) do
    Invoice.changeset(invoice, attrs)
  end

  def create_invoice(attrs, user_id) do
    incomes =
      attrs
      |> Map.get("incomes", [])
      |> get_incomes_by_ids(user_id)

    %Invoice{incomes: incomes, user_id: user_id}
    |> Invoice.changeset(attrs)
    |> Repo.insert()
  end

  def delete_invoice(id, user_id) do
    id
    |> get_invoice(user_id)
    |> Repo.delete()
  end
end
