defmodule FinanceWeb.IncomeLive.Index do
  use FinanceWeb, :live_view

  alias Finance.Invoices

  def mount(_params, %{"user_id" => user_id}, socket) do
    socket = assign(socket, user_id: user_id, incomes: Invoices.get_incomes_for_user(user_id))
    {:ok, socket, temporary_assigns: [incomes: nil]}
  end

  def handle_event("delete", %{"id" => id}, socket) do
    {:ok, _} = Invoices.delete_income(id, socket.assigns.user_id)
    incomes = Invoices.get_incomes_for_user(socket.assigns.user_id)

    socket = assign(socket, incomes: incomes)

    {:noreply, socket}
  end
end
