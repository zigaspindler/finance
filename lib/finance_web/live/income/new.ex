defmodule FinanceWeb.IncomeLive.New do
  use FinanceWeb, :live_view

  alias Finance.{
    Invoices,
    Clients
  }

  alias Finance.Invoices.Income

  def mount(_params, %{"user_id" => user_id}, socket) do
    clients = Clients.get_clients_for_user(user_id)
    {:ok, assign(socket, changeset: Invoices.change_income(%Income{}), clients: clients)}
  end

  def handle_event("save", %{"income" => params}, socket) do
    case Invoices.create_income(params) do
      {:ok, _} ->
        {:noreply,
         push_redirect(socket, to: Routes.live_path(socket, FinanceWeb.IncomeLive.Index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        socket = assign(socket, changeset: changeset)
        {:noreply, socket}
    end
  end
end
