defmodule FinanceWeb.IncomeLive.Edit do
  use FinanceWeb, :live_view

  alias Finance.{
    Invoices,
    Clients
  }

  alias Finance.Invoices.Income

  def mount(%{"id" => id}, %{"user_id" => user_id}, socket) do
    clients = Clients.get_clients_for_user(user_id)
    income = Invoices.get_income(id, user_id)

    {:ok,
     assign(socket,
       income_id: income.id,
       user_id: user_id,
       changeset: Invoices.change_income(income),
       clients: clients
     )}
  end

  def handle_event("save", %{"income" => params}, %{assigns: assigns} = socket) do
    case Invoices.update_income(assigns.income_id, assigns.user_id, params) do
      {:ok, _} ->
        {:noreply,
         push_redirect(socket, to: Routes.live_path(socket, FinanceWeb.IncomeLive.Index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        socket = assign(socket, changeset: changeset)
        {:noreply, socket}
    end
  end
end
