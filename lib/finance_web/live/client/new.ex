defmodule FinanceWeb.ClientLive.New do
  use FinanceWeb, :live_view
  alias FinanceWeb.Router.Helpers, as: Routes

  alias Finance.Clients
  alias Finance.Clients.Client

  def mount(_params, %{"user_id" => user_id}, socket) do
    {:ok, assign(socket, changeset: Clients.change_client(%Client{}), user_id: user_id)}
  end

  def handle_event("save", %{"client" => params}, socket) do
    case Clients.create_client_for_user(params, socket.assigns.user_id) do
      {:ok, _} ->
        {:noreply,
         push_redirect(socket, to: Routes.live_path(socket, FinanceWeb.ClientLive.Index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        socket = assign(socket, changeset: changeset)
        {:noreply, socket}
    end
  end
end
