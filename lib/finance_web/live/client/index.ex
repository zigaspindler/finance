defmodule FinanceWeb.ClientLive.Index do
  use FinanceWeb, :live_view

  alias Finance.Clients

  def mount(_params, %{"user_id" => user_id}, socket) do
    socket = assign(socket, clients: Clients.get_clients_for_user(user_id))
    {:ok, socket, temporary_assigns: [clients: []]}
  end
end
