defmodule FinanceWeb.LoginLive do
  use Phoenix.LiveView, layout: {FinanceWeb.LayoutView, "live_auth.html"}
  use FinanceWeb, :live_view

  def mount(_param, _sesion, socket) do
    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <%= link "Sign in", to: Routes.auth_path(@socket, :request, "google") %>
    """
  end
end
