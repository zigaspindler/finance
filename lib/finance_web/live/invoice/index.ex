defmodule FinanceWeb.InvoiceLive.Index do
  use FinanceWeb, :live_view

  alias Finance.Invoices

  def mount(_params, %{"user_id" => user_id}, socket) do
    socket = assign(socket, user_id: user_id, invoices: Invoices.get_invoices_for_user(user_id))
    {:ok, socket, temporary_assigns: [invoices: nil]}
  end

  def handle_event("delete", %{"id" => id}, socket) do
    {:ok, _} = Invoices.delete_invoice(id, socket.assigns.user_id)
    invoices = Invoices.get_invoices_for_user(socket.assigns.user_id)

    socket = assign(socket, invoices: invoices)

    {:noreply, socket}
  end
end
