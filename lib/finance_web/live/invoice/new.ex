defmodule FinanceWeb.InvoiceLive.New do
  use FinanceWeb, :live_view

  alias Finance.{
    Clients,
    Invoices
  }

  alias Finance.Invoices.Invoice

  def mount(_params, %{"user_id" => user_id}, socket) do
    clients = Clients.get_clients_for_user(user_id)

    client_id =
      clients
      |> List.first()
      |> Map.fetch!(:id)

    incomes = Invoices.get_uninvoiced_incomes(client_id, user_id)

    socket =
      assign(
        socket,
        user_id: user_id,
        clients: clients,
        client_id: client_id,
        incomes: incomes,
        changeset: Invoices.change_invoice(%Invoice{})
      )

    {:ok, socket, temporary_assigns: [incomes: nil]}
  end

  def handle_event(
        "change",
        %{"_target" => ["invoice", "client_id"], "invoice" => %{"client_id" => client_id}},
        socket
      ) do
    incomes = Invoices.get_uninvoiced_incomes(client_id, socket.assigns.user_id)

    {:noreply, assign(socket, incomes: incomes, client_id: client_id)}
  end

  def handle_event("change", _, socket), do: {:noreply, socket}

  def handle_event("save", %{"invoice" => params}, socket) do
    case Invoices.create_invoice(params, socket.assigns.user_id) do
      {:ok, _} ->
        {:noreply,
         push_redirect(socket, to: Routes.live_path(socket, FinanceWeb.InvoiceLive.Index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        incomes =
          Invoices.get_uninvoiced_incomes(socket.assigns.client_id, socket.assigns.user_id)

        IO.inspect(changeset)

        socket = assign(socket, changeset: changeset, incomes: incomes)
        {:noreply, socket}
    end
  end
end
