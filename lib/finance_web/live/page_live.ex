defmodule FinanceWeb.PageLive do
  use FinanceWeb, :live_view

  alias Finance.Accounts

  def mount(_params, %{"user_id" => user_id}, socket) do
    current_user = Accounts.get_user(user_id)
    {:ok, assign(socket, text: "It works!", current_user: current_user)}
  end
end
