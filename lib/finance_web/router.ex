defmodule FinanceWeb.Router do
  use FinanceWeb, :router

  pipeline :base do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_root_layout, {FinanceWeb.LayoutView, :root}
  end

  pipeline :browser do
    plug :base
    plug FinanceWeb.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", FinanceWeb do
    pipe_through :browser

    live "/", PageLive

    live "/clients", ClientLive.Index
    live "/clients/new", ClientLive.New

    live "/incomes", IncomeLive.Index
    live "/incomes/new", IncomeLive.New
    live "/incomes/:id/edit", IncomeLive.Edit

    live "/invoices", InvoiceLive.Index
    live "/invoices/new", InvoiceLive.New
    # live "/invoices/:id/edit", InvoiceLive.Edit
  end

  scope "/auth", FinanceWeb do
    pipe_through :base

    live "/login", LoginLive

    get "/:provider", AuthController, :request
    get "/:provider/callback", AuthController, :callback
  end

  # Other scopes may use custom stacks.
  # scope "/api", FinanceWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser
      live_dashboard "/dashboard", metrics: FinanceWeb.Telemetry
    end
  end
end
