defmodule FinanceWeb.AuthController do
  use FinanceWeb, :controller

  plug Ueberauth

  alias Finance.Accounts
  alias FinanceWeb.Auth

  def callback(%{assigns: %{ueberauth_failure: _fails}} = conn, _params) do
    conn
    |> put_flash(:error, "Failed to authenticate.")
    |> redirect(to: "/")
  end

  def callback(%{assigns: %{ueberauth_auth: ueberauth_auth}} = conn, params) do
    user = Accounts.find_or_create_user_by_email(ueberauth_auth.extra.raw_info.user)

    conn
    |> Auth.login(user)
    |> redirect(to: "/")
  end
end
