defmodule FinanceWeb.Auth do
  import Plug.Conn
  import Phoenix.Controller

  alias Finance.Accounts
  alias FinanceWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(conn, _opts) do
    case Plug.Conn.get_session(conn, :user_id) do
      nil ->
        conn
        |> redirect(to: "/auth/login")
        |> Plug.Conn.halt()

      _ ->
        conn
    end
  end

  def login(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end
end
