# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :finance,
  ecto_repos: [Finance.Repo]

# Configures the endpoint
config :finance, FinanceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "9ki78KO6dPFhvTzJD6MSQMN/QPszcmT67rOMWcf1MuDL0MhC22vcQgGKkCfIqIcp",
  render_errors: [view: FinanceWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Finance.PubSub,
  live_view: [signing_salt: "WKJ2OXsx"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :ueberauth, Ueberauth,
  providers: [
    google: {Ueberauth.Strategy.Google, [default_scope: "email profile"]}
  ]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
